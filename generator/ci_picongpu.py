# Copyright 2020 Simeon Ehrig
#
# ISC Software License

import sys

import hpccm
from hpccm.primitives import baseimage
import generator as gn


def main():
    if len(sys.argv) < 2:
        print(
            "Please add the base image name\n" 'python ci_picongpu.py "baseimage"',
            file=sys.stderr,
        )
        exit(1)

    stage = hpccm.Stage()
    stage.baseimage(image=sys.argv[1], _distro="ubuntu")

    for intr in gn.recipe_generator.get_picongpu_layer():
        stage += intr

    print(stage)


if __name__ == "__main__":
    main()
